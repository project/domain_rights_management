<?php 
/**
 * @file
 * Holds definitions needed for this module to work together with
 * the views-module.
 */

/**
 * Implements hook_views_data().
 *
 * @author cupcakemuncher
 * @name domain_rights_management_views_data
 */
function domain_rights_management_views_data() {
  $data = array();

  // Tabledefinitions
  // Table1
  $data['domain_rights_management']['table']['group'] = t('Domain Rights Management');
  $data['domain_rights_management']['table']['title'] = t('Basic Table');
  $data['domain_rights_management']['table']['help']  = t('This table defines the roles allowed for local administrators by Domain.');
  // Table2
  $data['domain_rights_management_exception_for_users']['table']['group'] = t('Domain Rights Management');
  $data['domain_rights_management_exception_for_users']['table']['title'] = t('User Exception Table');
  $data['domain_rights_management_exception_for_users']['table']['help']  = t('This table defines roles certain users may edit.');

  // Handler-definition
  $data['domain_rights_management_exception_for_users']['uid'] = array(
    'title'       => t('user id'),
    'help'       => t('The user that has exceptions.'),
    'filter'      => array(
      'title'   => t('DRM filter. Filtering by UID and DOMAIN_ID settings.'),
      // Defined in /views/user/views_plugin_argument_default_current_user.inc .
      'handler' => 'DomainRightsManagementViewsHandlerFilterUidDomainid',
    ),
  );

  // Join-definition
  $data['domain_rights_management']['table']['join']['users'] = array(
    'left_table' => 'users_roles',
    'left_field' => 'rid',
    'field'      => 'rid',
  );
  $data['domain_rights_management_exception_for_users']['table']['join']['users'] = array(
    'left_table' => 'users_roles',
    'left_field' => 'rid',
    'field'      => 'rid',
  );

  return $data;
}
