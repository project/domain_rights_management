<?php
/**
 * @file
 * Examples for implementing the hooks defined by this module.
 */

/**
 * Alters form used in "Domain Rights Management" to suite needs of deployment.
 * Intention of this hook is to allow for changes specific to 
 * the usermanagement(attributes in the users account) specific
 * to your drupalinstallation or domains running on it.
 *
 * @author cupcakemuncher
 * @name hook_domain_rights_management_alter_create_user
 *
 * @param array() $form
 *   The form to be altered
 * @param array() $form_state
 *   The state of the form.
 *
 * @return array()
 *   the altered from-array.
 */
function hook_domain_rights_management_create_user($form, $form_state) {
  drupal_set_message('<pre>' . print_r($form, 1) . '</pre>');
  return $form;
}
/**
 * Same as above except that it is for altering existing users.
 *
 * @author cupcakemuncher
 * @name hook_domain_rights_management_alter_user
 * 
 * @param array() $form
 *   The form-array to be altered.
 *   
 * @param array() $form_state
 *   The state of the form.
 *   
 * @return array()
 *   The altered form-array.
 */
function hook_domain_rights_management_alter_user($form, $form_state) {
  drupal_set_message('<pre>' . print_r($form, 1) . '</pre>');
  return $form;
}
