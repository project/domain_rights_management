Hi!
This module is an attempt to make user-administration
specific to domains.
Due to the workings of the "Domain Access"-module it is highly 
discouraged to assign multiple domains to  one user, if this user 
is administered by local administrators.
Local administrators are users chosen by the root-administrator or an 
equally powerful administrative group to help with the
user-administration on a domainlevel.
Thus easing the work generated by drupal-instances hosting multiple domains,
once they aquire a large userbase.


Editing users is sort of  clumsy since the API for users
performs additional checks. This leads to the module having to use a 
rights escalation to aquire the forms necessary.



------------------------------------------------------
            USAGE
------------------------------------------------------
Once installed you have to define the roles
which should be editable on a certain domain or by
a certain user ( admin/structure/domain/domainusermanagment ) .

Only users which hold no roles that are not defined for this domain or
local administrators are visible to the local administrator.
It is only possible to add rights.
You cannot take away rights from the local administrator using the 
usersetting and the domainsetting.

You then elect a local administrator by granting the user the right 
'Creating local users' .

Now the local administrator can use the form at 
admin/useradministration to administrate the users of his domain.


--------------------------------------------
      Possible ISSUES
--------------------------------------------
The module has been refactored.
All tests show that the refactoring went well.
However if an issue occures(e.g. function not found), please make it public.
