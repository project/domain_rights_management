<?php
/**
 * @file
 * Holds functions which are used by the modul as a whole
 * and logic which shouldn't be in the formbuilders.
 */

/**
 * Queries all the general conditions.
 *
 * @author cupcakemuncher
 * @name domain_rights_management_list_general_rights
 * @return array
 *   Returns rids of roles chosen for the general purpose of this module.
 */
function domain_rights_management_list_general_rights() {
  $query = NULL;
  $query = db_select('domain_rights_management_general_conditions', 'DRMGC');
  $query->addField('DRMGC', 'rid');
  $result = $query->execute();

  $retval = array();
  foreach ($result as $entry) {
    $retval[] = $entry->rid;
  }
  return $retval;
}

/**
 * Deletes all roles granted to all domains.
 *
 * @author cupcakemuncher
 * @name domain_rights_management_flush_rights_domain()
 */
function domain_rights_management_flush_rights_domain() {
  $query = db_delete('domain_rights_management');
  $query->condition('domain_id', 0, '>')->execute();
}


/**
 * Inserts or deletes general rights.
 *
 * @author cupcakemuncher
 * @name  domain_rights_management_insert
 *
 * @param array $rids
 *   Mapping rids and conditions of them.
 */
function domain_rights_management_insert_delete_general_rights(&$rids) {
  foreach ($rids as $value => $chosen) {
    $query = db_select('domain_rights_management_general_conditions', 'DRMGC');
    $query->addField('DRMGC', 'rid');
    $result = $query->condition('rid', $value)->execute()->rowCount();

    if ($chosen == $value) {
      if ($result == NULL) {
        $newid = db_insert('domain_rights_management_general_conditions')
                 ->fields(array('rid' => $value))->execute();
      }
    }
    if (($chosen != $value) && $chosen == 0) {
      // Unique guarantees one occurence.
      if ($result != NULL) {
        db_delete('domain_rights_management_general_conditions')
        ->condition('rid', $value)->execute();
      }
    }
  }
}


/**
 * Lists all the users by domain-id.
 *
 * @author cupcakemuncher
 * @name domain_rights_management_list_users
 *
 * @param String $domainid
 *   integer representing the domainid
 *
 * @return array()
 *   mapping username and various informations about the user.
 */
function domain_rights_management_list_users($domainid) {
  $query = NULL;
  global $user;

  $uidlist = '(-1)';

  // Authenticated user.
  $tmp_roles = array(2);
  $tmp_roles = array_merge($tmp_roles, array_keys(domain_rights_management_query_exceptions_for_user($user->uid)));
  $tmp_roles = array_merge($tmp_roles, array_keys(domain_rights_management_list_roles_for_domain($domainid)));

  $user_uid = db_select('users', 'u');
  $user_uid->join('domain_editor', 'de', 'de.uid = u.uid');
  $user_uid->condition('de.domain_id', $domainid);
  $user_uid->addField('u', 'uid');

  $result = $user_uid->execute();
  $tmp_users = $result->fetchCol();

  $users = user_load_multiple($tmp_users);
  $tmp_users = array();
  $valid = FALSE;

  foreach ($users as $account) {
    $valid = TRUE;
    foreach ($account->roles as $key => $value) {
      if (!in_array($key, $tmp_roles)) {
        $valid = FALSE;
        break;
      }
    }
    if ($valid) {
      $tmp_users[] = $account->uid;
    }
  }

  if (count($tmp_users) > 0) {
    $uidlist = '(' . implode(',', $tmp_users) . ')';
  }

  $query = db_select('users', 'u');
  $query->where('u.uid in ' . $uidlist);
  $query->leftJoin('users_roles', 'ur', 'u.uid = ur.uid');
  $query->leftJoin('role', 'r', 'ur.rid = r.rid');

  $query->fields('u', array('uid', 'name', 'status', 'access'));
  $query->fields('r', array('name', 'rid'));
  $result = $query->execute();

  $retval = array();
  $dom = domain_get_domain();

  foreach ($result as $entry) {
    if (($entry->uid == 1)||($user->uid == $entry->uid)) {
      continue;
    }
    if (isset($retval[$entry->name]) &&
        is_array($retval[$entry->name]['role']) &&
        !in_array($entry->r_name, $retval[$entry->name]['role'])) {
      $retval[$entry->name]['role'][]   = $entry->r_name;
    }
    else {
      $retval[$entry->name] = array();
      $retval[$entry->name]['access']   = $entry->access;
      $retval[$entry->name]['status']   = $entry->status;
      if ($entry->r_name != '') {
        $retval[$entry->name]['role']   = array($entry->r_name);
      }
      else {
        $retval[$entry->name]['role']   = array();
      }
      $retval[$entry->name]['uid']      = $entry->uid;
    }
  }
  return $retval;
}

/**
 * Lists all Users who have been granted local admin privileges.
 *
 * @author cupcakemuncher
 * @name domain_rights_management_query_local_admins
 * @return array
 *   returns uid mapped to username.
 */
function domain_rights_management_query_local_admins() {
  $query = db_select('role_permission', 'rp');
  $query->distinct();
  $query->where('permission = :arg1', array(':arg1' => 'local_administration'));
  $query->join('users_roles', 'ur', 'ur.rid = rp.rid');
  $query->join('users', 'u', 'u.uid = ur.uid AND u.uid != 1');
  $query->fields('u', array('uid', 'name'));
  $result = $query->execute();
  $ret = array();
  foreach ($result as $entry) {
    $ret[$entry->uid] = $entry->name;
  }
  return $ret;
}

/**
 * Lists all roles the user is allowed to grant.
 *
 * @author cupcakemuncher
 * @name domain_rights_management_query_exceptions_for_user
 *
 * @param int $uid
 *   string containing the uid of the user
 *
 * @return array
 *   The roleid and the rolename
 */
function domain_rights_management_query_exceptions_for_user($uid) {
  $query = db_select('domain_rights_management_exception_for_users', 'DRMEFU');
  $query->condition('uid', $uid);
  $query->join('role', 'r', 'DRMEFU.rid = r.rid');
  $query->addField('DRMEFU', 'rid');
  $query->addField('r', 'name');
  $result = $query->execute();

  $ret = array();
  foreach ($result as $entry) {
    $ret[$entry->rid] = $entry->name;
  }

  return $ret;
}

/**
 * Inserts or deletes exceptions for the user, depending on the submitted form.
 *
 * @author cupcakemuncher
 * @name domain_rights_management_insert_delete_exceptions_for_user
 *
 * @param int $uid
 *   String containing the userid
 *   
 * @param array $rids
 *   array(string->string)
 */
function domain_rights_management_insert_delete_exceptions_for_user($uid, $rids) {
  foreach ($rids as $value => $chosen) {
    $query = db_select('domain_rights_management_exception_for_users', 'DRMEFU');
    $query->addField('DRMEFU', 'uid');
    $result = $query->condition('uid', $uid)
                    ->condition('rid', $value)->execute()->rowCount();

    if ($chosen == $value) {
      if ($result == NULL) {
        $newid = db_insert('domain_rights_management_exception_for_users')
                 ->fields(array('uid' => $uid, 'rid' => $value))->execute();
      }
    }
    if (($chosen != $value) && ($chosen == 0)) {
      // Unique guarantees one occurence.
      if ($result != NULL) {
        db_delete('domain_rights_management_exception_for_users')
          ->condition('rid', $value)
          ->condition('uid', $uid)
          ->execute();
      }
    }
  }
}


/**
 * Lists all the registered domains,wether inactive or not.
 *
 * @name domain_rights_management_query_local_domains
 * @author cupcakemuncher
 * @return array
 *   Array mapping domainid and additional infos about that domain.
 */
function domain_rights_management_query_local_domains() {
  $query = db_select('domain', 'd');
  $query->fields('d', array('domain_id', 'sitename', 'machine_name'));
  $result = $query->execute();
  $ret = array();
  foreach ($result as $entry) {
    $ret[$entry->domain_id] = array(
      'domainname' => $entry->sitename,
      'machinename' => $entry->machine_name);
  }
  return $ret;
}

/**
 * Inserts and deletes roles allowed to be assigned on a certain domain.
 *
 * @author cupcakemuncher
 * @name domain_rights_management_insert_delete_privilege()
 *
 * @param int $domain_id
 *   Domainid in question.
 * @param array $rid
 *   Array mapping roleid and rolename
 *
 * @return mixed 
 *   FALSE if insertion went wrong or entry already present 
 *   otherwise int rowid is returned
 */
function domain_rights_management_insert_delete_privilege($domain_id, &$rid) {
  foreach ($rid as $rightsid => $ischosen) {
    $select = db_select('domain_rights_management', 'DRM');
    $select = $select->fields('DRM')->condition('domain_id', $domain_id, '= ');
    $result = $select->condition('rid', $rightsid, '= ')->execute();
    $tmp = $result->rowCount();

    if (($tmp == NULL) && ($ischosen != 0)) {
      $insert = db_insert('domain_rights_management');
      $insert->fields(array('domain_id' => $domain_id, 'rid' => $rightsid));
      $insert->execute();
    }

    if ($tmp != NULL && $ischosen == 0) {
      $delete = db_delete('domain_rights_management');
      $delete = $delete->condition('domain_id', $domain_id);
      $delete = $delete->condition('rid', $rightsid);
      $res = $delete->execute();
    }
  }
}

/**
 * Lists all the roles the localadmin is allowed to assign.
 *
 * @author cupcakemuncher
 * @name domain_rights_management_list_roles_for_domain
 *
 * @param int $domain_id
 *   unsigned int  holding domainid
 *
 * @return array
 *   Returns rid-rolename-map.
 */
function domain_rights_management_list_roles_for_domain($domain_id) {
  $roles = array();
  global $user;

  $select = db_select('domain_rights_management', 'DRM')
            ->condition('DRM.domain_id', $domain_id, '=');
  $select->join('role', 'r', 'r.rid = DRM.rid');
  $select->fields('r', array('rid', 'name'));
  $result = $select->execute();

  foreach ($result as $row) {
    $roles[$row->rid] = $row->name;
  }
  return $roles;
}


/**
 * Deletes the user.
 *
 * @author cupcakemuncher
 * @name domain_rights_management_delete_user
 *
 * @param int $uid
 *   unsigned int holding userid
 *
 * @return bool
 *   Announced failure or success of delete-operation.
 */
function domain_rights_management_delete_user($uid) {
  module_load_include('inc', 'node', 'node.admin');

  $domain = domain_get_domain();
  global $user;

  if (!domain_rights_management_is_user_assigned_to_current_domain($user->uid)) {
    return FALSE;
  }

  db_delete('domain_editor')
  ->condition('uid', $uid)
  ->condition('domain_id', $domain['domain_id'])
  ->execute();

  $result = db_select('domain_editor', 'de')
            ->fields('de', array('uid'))
            ->condition('uid', $uid, '= ')->execute();
  if ($result->rowCount() == 0) {
    // Taken from  node.module function() node_user_cancel()
    $nodes = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('uid', $uid)
    ->execute()
    ->fetchCol();
    // All content assigned to anonymous.
    node_mass_update($nodes, array('uid' => 0));

    user_delete($uid);
  }
  return TRUE;
}

/**
 * Allows the lokal admin to block a user.
 *
 * @author cupcakemuncher
 * @name domain_rights_management_block_user
 *
 * @param int $uid
 *   Holds uid.
 *
 * @return bool
 *   Returns success of failure of the blockoperation.
 */
function domain_rights_management_block_user($uid) {
  if (!domain_rights_management_is_user_allowed()) {
    return FALSE;
  }

  global $user;
  if (($uid != 1) && !($user->uid == $uid)) {
    $tmp = user_load($uid);
    user_block_user_action($tmp);
    return TRUE;
  }
  return FALSE;
}


/**
 * Allowes the lokal admin to unblock a user.
 *
 * @author cupcakemuncher
 * @name domain_rights_management_unblock_user
 *
 * @param int $uid
 *   Holding uid.
 *
 * @return bool
 *   determins if the operation of unblocking the user as
 *   successful or not.
 */
function domain_rights_management_unblock_user($uid) {
  if (domain_rights_management_is_user_allowed()) {
    $tmp = array(user_load($uid));
    user_user_operations_unblock(array($uid));
    return TRUE;
  }
  return FALSE;
}
