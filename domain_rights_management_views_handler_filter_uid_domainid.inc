<?php
/**
 * @file
 * Holds Handlerdefinition needed by the views-module.
 */

/**
 * @author cupcakemuncher
 * @class domain_rights_management_views_handler_filter_uid
 * @extends views_handler_filter
 * @see http://views.doc.logrus.com/
 */
class DomainRightsManagementViewsHandlerFilterUidDomainid extends views_handler_filter {
  protected $allowedUsers;

  /**
   * Method defined in parentclass(non-PHPdoc) .
   * @see views_handler_filter::init()
   */
  public function init(&$view, $option) {
    parent::init($view, $option);

    module_load_include('inc', 'domain_rights_management', 'includes/helper_functions');

    $domain = domain_get_domain();

    $tmp_users = array();

    $tmp_userlist = domain_rights_management_list_users($domain['domain_id']);

    foreach ($tmp_userlist as $key => $value) {
      $tmp_users[] = $value['uid'];
    }

    if (count($tmp_users) > 0) {
      $this->allowedUsers = '(' . implode(',', $tmp_users) . ')';
    }
    else {
      $this->allowedUsers = '(-1)';
    }
  }

  /**
   * Method defined in parentclass(non-PHPdoc) .
   * @see views_handler_filter::query()
   */
  public function query() {
    $this->ensure_my_table();
    global $user;
    // Following code was inspired by
    // views/modules/user/views_handler_filter_user_current.inc
    $field = $this->table_alias . '.' . $this->real_field . ' ';
    $or = db_or();
    $or->where('{users}.uid in ' . $this->allowedUsers);
    $this->query->add_where($this->options['group'], $or);
  }
}
